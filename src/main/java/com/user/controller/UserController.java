package com.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user.dto.UserDto;
import com.user.service.UserService;

/**
 * @author seshananda
 *
 */
@RestController

public class UserController {
	
	

	@Autowired
	UserService userService;

	/**
	 * 
	 * @param userDto
	 * @return
	 */
	@PostMapping("/userCreating")
	public ResponseEntity<String> createUser(@RequestBody @Valid UserDto userDto) {
		userService.createUser(userDto);
		return new ResponseEntity<String>("User Added Sucessfully", HttpStatus.CREATED);

	}
	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */

	@GetMapping("/getAllUsers")
	public ResponseEntity<List<UserDto>> getAll(int pageNumber,int pageSize){
		List<UserDto> list= userService.getAll(pageNumber,pageSize);
		return new ResponseEntity<List<UserDto>>(list,HttpStatus.OK) ;
		
	}
	/**
	 * 
	 * @param userDto
	 * @param uId
	 * @return
	 */
	
	@PutMapping("/updateUser")
	public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto,@RequestParam Long uId){
		userService.updateUser(userDto,uId);
		return new ResponseEntity<UserDto> (HttpStatus.CREATED);
		
	}
	/**
	 * 
	 * @param uId
	 * @return
	 */
	
	@DeleteMapping("/deleteUser")
	public ResponseEntity<String> deleteUser(@RequestParam Long uId){
		userService.deleteUser(uId);
		return new ResponseEntity<String>("User Deleted Succesfully",HttpStatus.CONTINUE);
		
	}
	

}

package com.user.dto;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.user.entity.Role;

public class UserDto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Digits(integer = 10, fraction = 1, message = "user Code should be less than 10 digits")
	private Long uId;
	
	@NotEmpty(message = "Name must not be Empty")
	@Size(max=50)
	@Column(name="Name")
	private String Name;
	
	@NotEmpty(message = "userName must not be Empty")
	@Size(max=50)
	@Column(name="userName")
	private String userName;
	
	@NotEmpty(message = "password must not be Empty")
	@Size(max=50)
	@Column(name="password")
	private String password;
	
	@Enumerated(value=EnumType.STRING)
	@Column(name="role")
	private Role role;

	public Long getuId() {
		return uId;
	}

	public void setuId(Long uId) {
		this.uId = uId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	

}

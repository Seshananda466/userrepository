package com.user.service;

import java.util.List;

import com.user.dto.UserDto;

public interface UserService {

	/**
	 * 
	 * @param user
	 * @return
	 */
	String createUser(UserDto userDto);
	/**
	 * 
	 * @param pagesize
	 * @param pageNumber
	 * @return
	 */
	List<UserDto> getAll(int pageNumber,int pageSize);
	/**
	 * 
	 * @param userDto
	 * @param uId
	 * @return 
	 */
	UserDto updateUser(UserDto userDto, Long uId);
	/**
	 * 
	 * @param uId
	 */
	String deleteUser(Long uId);
	
	
}

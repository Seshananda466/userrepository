package com.user.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.user.dto.UserDto;
import com.user.entity.User;
import com.user.repository.UserRepository;
import com.user.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;

	@Override
	public String createUser(UserDto userDto) {
		User us = new User();
		BeanUtils.copyProperties(userDto, us);
		 logger.info("user Added Successfully");
		 userRepository.save(us);
		 return "user added successfully";
	}

	@Override
	public List<UserDto> getAll(int pageNumber,int pageSize) {
		Page<User> user;
		List<UserDto> userDto = new ArrayList<>();
		Pageable pageable = PageRequest.of( pageNumber,pageSize);
		user= userRepository.findAll(pageable);
		user.stream().forEach(us -> {
			UserDto employeeDetailsDto = new UserDto();
			BeanUtils.copyProperties(us, employeeDetailsDto);
			userDto.add(employeeDetailsDto);
		});
		return userDto;
	}

	@Override
	public UserDto updateUser(UserDto userDto, Long uId) {
		Optional<User> use;
		use = userRepository.findById(uId);
		User userInfo = use.get();
		BeanUtils.copyProperties(userDto, userInfo);
		UserDto usDto = new UserDto();
		BeanUtils.copyProperties(userRepository.save(userInfo),usDto);
		return usDto;
	}

	@Override
	public String deleteUser(Long uId) {
		Optional<User> use= userRepository.findById(uId);
		User userInfo = use.get();
		userRepository.delete(userInfo);
		return "User Deleted";
	}

	

	
}



